/*
 * Copyright 2018 TridentMC (Benjamin K, Ethan Brooks)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.compound.gui.widget;

import com.tridevmc.compound.gui.CompoundGui;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public abstract class WidgetBase {

    protected int top, left, width, height;
    protected CompoundGui parent;

    public final int getTop() {
        return this.top;
    }

    public final int getLeft() {
        return this.left;
    }

    public final void setPosition(int top, int left) {
        this.top = top;
        this.left = left;
    }

    public final int getWidth() {
        return this.width;
    }

    public final void setWidth(int width) {
        this.width = width;
    }

    public final int getHeight() {
        return this.height;
    }

    public final void setHeight(int height) {
        this.height = height;
    }

    public void onRegister(CompoundGui gui) {
        this.parent = gui;
    }

    public void drawBackground(int mouseX, int mouseY) {
    }

    public void drawForeground(int mouseX, int mouseY) {

    }
}
