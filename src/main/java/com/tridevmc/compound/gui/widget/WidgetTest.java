/*
 * Copyright 2018 TridentMC (Benjamin K, Ethan Brooks)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.compound.gui.widget;

import net.minecraft.client.gui.Gui;

public class WidgetTest extends WidgetBase {

    int current = 0;
    int currentTimer = 20;

    @Override
    public void drawForeground(int mouseX, int mouseY) {
//		this.parent.drawBeveledBoxes(24,24,18,18, 9, 4);

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 9; j++) {
                this.parent.drawBeveledBox(24 + j * (18), 24 + (i * 18), 18, 18);
                if ((i * 9) + j == current) {
                    //GlStateManager.disableLighting();
                    //GlStateManager.disableDepth();
                    //GlStateManager.colorMask(true, true, true, false);
                    Gui.drawRect(24 + j * (18) + 1, 24 + i * (18), 24 + j * (18) + 17,
                        24 + i * (18) + 17, -2130706433);
                    //GlStateManager.colorMask(true, true, true, true);
                    //GlStateManager.enableLighting();
                    //GlStateManager.enableDepth();
                    currentTimer--;
                    if (currentTimer == 0) {
                        currentTimer = 5;

                        if (++current == 4 * 9) {
                            current = 0;
                        }
                    }
                }
            }
        }
    }
}
