/*
 * Copyright 2018 TridentMC (Benjamin K, Ethan Brooks)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.compound.network.core;

import com.tridevmc.compound.network.message.Message;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraftforge.fml.common.network.FMLIndexedMessageToMessageCodec;

public class CompoundIndexedCodec extends FMLIndexedMessageToMessageCodec<Message> {

    private final CompoundNetwork network;

    public CompoundIndexedCodec(CompoundNetwork network) {
        this.network = network;
    }

    @Override
    public void encodeInto(ChannelHandlerContext ctx, Message msg, ByteBuf target) {
        this.network.getMsgConcept(msg).toBytes(msg, target);
    }

    @Override
    public void decodeInto(ChannelHandlerContext ctx, ByteBuf source, Message msg) {
        this.network.getMsgConcept(msg).fromBytes(msg, source);
    }
}
