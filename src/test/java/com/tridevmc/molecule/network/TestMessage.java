/*
 * Copyright 2018 TridentMC (Benjamin K, Ethan Brooks)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.molecule.network;

import com.tridevmc.compound.network.message.Message;
import com.tridevmc.molecule.Molecule;
import java.util.Random;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import org.apache.commons.lang3.builder.MultilineRecursiveToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class TestMessage extends Message {

    public float myFloat;
    public double myDouble;
    public byte myByte;
    public short myShort;
    public int myInt;
    public long myLong;

    public boolean myBoolean;
    public boolean myOtherBoolean;
    public char myChar;
    public String myString;

    public ItemStack myItemStack;
    public NBTTagCompound myTag;
    public BlockPos myPos;

    public TestMessage() {
        this(false);
    }

    public TestMessage(boolean genValues) {
        if (genValues) {
            this.myFloat = Float.MAX_VALUE;
            this.myDouble = Double.MAX_VALUE;
            this.myByte = Byte.MAX_VALUE;
            this.myShort = Short.MAX_VALUE;
            this.myInt = Integer.MAX_VALUE;
            this.myLong = Long.MAX_VALUE;

            this.myBoolean = true;
            this.myOtherBoolean = false;
            this.myChar = 'a';
            this.myString = "haha YES";

            this.myItemStack = Item.REGISTRY.getRandomObject(new Random()).getDefaultInstance();
            this.myTag = new NBTTagCompound();
            this.myTag.setFloat("Float", 4F);
            Random rand = new Random();
            this.myPos = new BlockPos(rand.nextInt(511), rand.nextInt(255), rand.nextInt(511));

            Molecule.LOG.info("Sending {}, {}", this.getClass().getName(), this.toString());
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, new MultilineRecursiveToStringStyle())
            .append("myFloat", myFloat)
            .append("myDouble", myDouble)
            .append("myByte", myByte)
            .append("myShort", myShort)
            .append("myInt", myInt)
            .append("myLong", myLong)
            .append("myBoolean", myBoolean)
            .append("myOtherBoolean", myOtherBoolean)
            .append("myChar", myChar)
            .append("myString", myString)
            .append("myItemStack", myItemStack.getItem() + ":" + myItemStack.getMetadata())
            .append("myTag", myTag)
            .append("myPos", myPos)
            .toString();
    }

    @Override
    public void handle(EntityPlayer player) {
        Molecule.LOG.info("Received {}, {}", this.getClass().getName(), this.toString());
    }
}
